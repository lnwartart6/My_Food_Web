import { Fragment } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import classes from "./FoodPage.module.css";

import picture from "../../resources/pictures/menuPage_blur.jpg";
import TopImg from "../../components/TopImg/TopImg";
import Food from "../../components/Food/Food";

const FoodPage = () => {
  const params = useParams();
  const { typeFood, id } = params;
  const dataTypeFood = useSelector((state) => state.menu[typeFood]);
  const food = dataTypeFood.find((tFood) => tFood.id === id);
  return (
    <Fragment>
      <TopImg picture={picture} />
      <h1 className={classes.nameFood}>
        Home &#62; Our Menu &#62; {food.name}
      </h1>
      <Food dataFood={food} />
    </Fragment>
  );
};

export default FoodPage;
