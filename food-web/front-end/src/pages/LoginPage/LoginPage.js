import { useRef } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";

import { loginApi } from "../../api/api";

import { userLoggingIn } from "../../components/store/actions/userActions";

import classes from "./LoginPage.module.css";
import ImgBg from "../../components/TopImg/ImgBg";
const LoginPage = () => {
  const usernameRef = useRef();
  const passwordRef = useRef();
  const history = useHistory();
  const dispatch = useDispatch();
  const loginHandler = (event) => {
    event.preventDefault();
    const userLogin = {
      username: usernameRef.current.value,
      password: passwordRef.current.value,
    };
    loginApi(userLogin).then((res) => {
      res.json().then((data) => {
        if (data.message) {
          alert(data.message);
          return;
        }
        dispatch(userLoggingIn(data, history));
        history.push("/home");
      });
    });
  };
  const registerHandler = () => {
    history.push("/register");
  };
  return (
    <div className={classes.loginPageContainer}>
      <ImgBg />
      <form className={classes.formContainer} onSubmit={loginHandler}>
        <h1>Login</h1>
        <div className={classes.loginContainer}>
          <label>Username</label>
          <input type="text" ref={usernameRef} required />
        </div>
        <div className={classes.loginContainer}>
          <label>Password</label>
          <input type="password" ref={passwordRef} required />
        </div>
        <div className={classes.buttonContainer}>
          <button>Log In</button>
          <button type="button" onClick={registerHandler}>
            Register
          </button>
        </div>
      </form>
    </div>
  );
};

export default LoginPage;
