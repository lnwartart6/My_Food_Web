import { useRef } from "react";
import { useDispatch } from "react-redux";

import { userLoggingIn } from "../../components/store/actions/userActions";
import { registerApi } from "../../api/api";
import ImgBg from "../../components/TopImg/ImgBg";
import classes from "./RegisterPage.module.css";
const RegisterPage = () => {
  const usernameRef = useRef();
  const passwordRef = useRef();
  const nameRef = useRef();
  const addressRef = useRef();
  const telRef = useRef();
  const dispatch = useDispatch();
  const registerHandler = (event) => {
    event.preventDefault();
    const newUser = {
      username: usernameRef.current.value,
      password: passwordRef.current.value,
      name: nameRef.current.value,
      tel: telRef.current.value,
      address: addressRef.current.value,
    };
    registerApi(newUser).then((res) => {
      res.json().then((data) => {
        if (data.message) {
          alert(data.message);
          return;
        }
        dispatch(userLoggingIn(data));
      });
    });
  };
  return (
    <div className={classes.registerPageContainer}>
      <ImgBg />
      <form className={classes.formContainer} onSubmit={registerHandler}>
        <h1>Register</h1>
        <div className={classes.loginContainer}>
          <label>Username</label>
          <input type="text" ref={usernameRef} required />
        </div>
        <div className={classes.loginContainer}>
          <label>Password</label>
          <input type="password" ref={passwordRef} required />
        </div>
        <div className={classes.loginContainer}>
          <label>Name</label>
          <input type="text" ref={nameRef} required />
        </div>
        <div className={classes.loginContainer}>
          <label>Tel.</label>
          <input type="text" ref={telRef} required />
        </div>
        <div className={classes.loginContainer}>
          <label>Address</label>
          <textarea type="text" ref={addressRef} required />
        </div>
        <div className={classes.buttonContainer}>
          <button>Submit</button>
        </div>
      </form>
    </div>
  );
};

export default RegisterPage;
