import { Fragment } from "react";
import Intro from "../../components/Intro/Intro";
import MiniMenu from "../../components/MiniMenu/MiniMenu";
import RecommendMenu from "../../components/RecommendMenu/RecommendMenu";

const HomePage = () => {
  return (
    <Fragment>
      <Intro />
      <MiniMenu />
      <RecommendMenu />
    </Fragment>
  );
};

export default HomePage;
