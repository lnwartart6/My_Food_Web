import { useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { errorActions } from "../../components/store/errorMessageSlice";
import { userActions } from "../../components/store/userSlice";
import { updateProfileApi } from "../../api/api";
import CustomError from "../../functions/CustomError";
import classes from "./ProfilePage.module.css";
import ImgBg from "../../components/TopImg/ImgBg";

const ProfilePage = (props) => {
  const userInfo = useSelector((state) => state.user);
  const { name, tel, address, username, isUserLogin } = userInfo;
  const nameRef = useRef();
  const telRef = useRef();
  const addressRef = useRef();
  const passwordRef = useRef();
  const dispatch = useDispatch();
  const [isUserEdit, setIsUserEdit] = useState(false);
  const updateProfileHandler = (e) => {
    e.preventDefault();
    const idToken = JSON.parse(localStorage.getItem("userToken"));
    const updateInfo = {
      username,
      idToken,
      isUserLogin,
      name: nameRef.current.value,
      tel: telRef.current.value,
      address: addressRef.current.value,
      password: passwordRef.current.value,
    };
    updateProfileApi(updateInfo).then((res) => {
      res
        .json()
        .then((data) => {
          if (data.message) {
            throw new CustomError(403, data.message);
          } else {
            dispatch(userActions.loginUser(updateInfo));
            setIsUserEdit((prevState) => !prevState);
          }
        })
        .catch((e) => {
          dispatch(errorActions.addError(e.message));
          alert(e.message);
        });
    });
  };
  const editProfileHandler = () => {
    setIsUserEdit((prevState) => !prevState);
  };
  return (
    <div className={classes.profilePageContainer}>
      <ImgBg />
      <form className={classes.formContainer} onSubmit={updateProfileHandler}>
        <h1>Profile</h1>
        <div className={classes.loginContainer}>
          <label>Username</label>
          <span>{username}</span>
        </div>
        <div className={classes.loginContainer}>
          <label>Name</label>
          {isUserEdit ? (
            <input type="text" ref={nameRef} defaultValue={name} />
          ) : (
            <span>{name}</span>
          )}
        </div>
        <div className={classes.loginContainer}>
          <label>Tel.</label>
          {isUserEdit ? (
            <input type="text" ref={telRef} defaultValue={tel} />
          ) : (
            <span>{tel}</span>
          )}
        </div>
        <div className={classes.loginContainer}>
          <label>Address</label>
          {isUserEdit ? (
            <textarea type="text" ref={addressRef} />
          ) : (
            <textarea readOnly="readonly" defaultValue={address} />
          )}
        </div>
        {isUserEdit && (
          <div className={classes.loginContainer}>
            <label>Enter your password</label>
            <input type="password" ref={passwordRef} />
          </div>
        )}
        <div className={classes.buttonContainer}>
          <button type="button" onClick={editProfileHandler}>
            {isUserEdit ? "Cancel" : "Edit Profile"}
          </button>
          {isUserEdit && <button>Submit</button>}
        </div>
      </form>
    </div>
  );
};

export default ProfilePage;
