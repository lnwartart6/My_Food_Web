import { Fragment } from "react";
import { useSelector } from "react-redux";
import TopImg from "../../components/TopImg/TopImg";
import picture from "../../resources/pictures/introBg.jpg";
import classes from "./ErrorPage.module.css";

const ErrorPage = (props) => {
  const errorMessage =
    useSelector((state) => state.error.message) || "Page Not Found!";
  return (
    <Fragment>
      <TopImg picture={picture} />
      <div className={classes.errorMessageContainer}>
        <div className="alert alert-danger" role="alert">
          {errorMessage}!
        </div>
      </div>
    </Fragment>
  );
};

export default ErrorPage;
