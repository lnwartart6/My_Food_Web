import { Fragment } from "react";
import TopImg from "../../components/TopImg/TopImg";
import Menu from "../../components/Menu/Menu";
import picture from "../../resources/pictures/menuPage_blur.jpg";

const MenuPage = () => {
  return (
    <Fragment>
      <TopImg picture={picture} />
      <Menu />
    </Fragment>
  );
};

export default MenuPage;
