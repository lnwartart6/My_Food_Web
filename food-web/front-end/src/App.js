import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, Route, Switch, useHistory } from "react-router-dom";

import { getMenuData } from "./components/store/menuSlice";
import { getOrderInCart } from "./components/store/cartOrderSlice";
import { getUserLogIn } from "./components/store/actions/userActions";
import { errorActions } from "./components/store/errorMessageSlice";

import "./App.css";
import Layout from "./components/Layout/Layout";
import HomePage from "./pages/HomePage/HomePage";
import MenuPage from "./pages/MenuPage/MenuPage";
import FoodPage from "./pages/FoodPage/FoodPage";
import LoginPage from "./pages/LoginPage/LoginPage";
import RegisterPage from "./pages/RegisterPage/RegisterPage";
import ErrorPage from "./pages/ErrorPage/ErrorPage";
import ProfilePage from "./pages/ProfilePage/ProfilePage";

function App() {
  const [firstRender, setFirstRender] = useState(true);
  const isUserLogin = useSelector((state) => state.user.isUserLogin);
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    dispatch(getMenuData())
      .then(() => {
        dispatch(getUserLogIn(history));
        dispatch(getOrderInCart());
      })
      .then(() => {
        setFirstRender(false);
      })
      .catch((e) => {
        dispatch(errorActions.addError(e.message));
        setFirstRender(false);
        history.push("/error");
      });
  }, []);
  if (firstRender) {
    return <div>Loading...</div>;
  } else if (!firstRender) {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact>
            <Redirect to="/home" />
          </Route>
          <Route path="/home">
            <HomePage />
          </Route>
          <Route path="/login">
            <LoginPage />
          </Route>
          <Route path="/register">
            <RegisterPage />
          </Route>
          {isUserLogin && (
            <Route path="/profile">
              <ProfilePage />
            </Route>
          )}
          <Route path="/menu" exact>
            <MenuPage />
          </Route>
          <Route path="/menu/:typeFood/:id">
            <FoodPage />
          </Route>
          <Route path="*">
            <ErrorPage />
          </Route>
        </Switch>
      </Layout>
    );
  }
}

export default App;
