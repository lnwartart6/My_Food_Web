export const registerApi = async (newUser) => {
  return fetch("http://localhost:4000/register", {
    method: "POST",
    body: JSON.stringify(newUser),
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const updateProfileApi = async (updateInfo) => {
  return fetch("http://localhost:4000/profile", {
    method: "PATCH",
    body: JSON.stringify(updateInfo),
    headers: {
      "Content-type": "application/json",
    },
  });
};

export const loginApi = async (userLogin) => {
  return fetch("http://localhost:4000/login", {
    method: "POST",
    body: JSON.stringify(userLogin),
    headers: {
      "Content-Type": "application/json",
    },
  });
};
