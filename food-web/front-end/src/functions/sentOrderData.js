import { cartOrderActions } from "../components/store/cartOrderSlice";
import { sentUserOrder } from "../components/store/cartOrderSlice";
import { errorActions } from "../components/store/errorMessageSlice";

const sentOrderData = (sentOrder, dispatch, closeCartHandler, history) => {
  dispatch(sentUserOrder(sentOrder))
    .then(() => {
      dispatch(cartOrderActions.clearFood());
    })
    .then(() => {
      closeCartHandler();
    })
    .catch((e) => {
      dispatch(errorActions.addError(e.message));
      console.log(e.message);
      closeCartHandler();
      history.push("/error");
    });
};

export default sentOrderData;
