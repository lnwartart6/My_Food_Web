import classes from "./Overlay.module.css";

const Overlay = (props) => {
  return <div className={classes.overlay} onClick={props.onClickOverlay}></div>;
};

export default Overlay;
