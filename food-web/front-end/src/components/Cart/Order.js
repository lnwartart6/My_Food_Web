import classes from "./Order.module.css";

import { useDispatch } from "react-redux";
import { cartOrderActions } from "../store/cartOrderSlice";

const Order = (props) => {
  const { name, price, amount, id } = props;
  const dispatch = useDispatch();
  const onIncreaseFood = () => {
    dispatch(cartOrderActions.increaseFood(id));
  };
  const onDecreaseFood = () => {
    dispatch(cartOrderActions.decreaseFood(id));
  };
  return (
    <div className={classes.orderContainer}>
      <div className={classes.detailOrderContainer}>
        <h2>{name}</h2>
        <h3>
          {price}B <span>X{amount}</span>
        </h3>
      </div>
      <div className={classes.numberOrderContainer}>
        <button onClick={onIncreaseFood}>&#43;</button>
        <button onClick={onDecreaseFood}>&#8722;</button>
        <h4>{price * amount} B</h4>
      </div>
    </div>
  );
};

export default Order;
