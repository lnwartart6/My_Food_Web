import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { cartToggleAction } from "../store/cartToggleSlice";
import sentOrderData from "../../functions/sentOrderData";

import Order from "./Order";
import classes from "./Cart.module.css";
import ClientForm from "./ClientForm";

const Cart = () => {
  const [triggerOrder, setTriggerOrder] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  const orders = useSelector((state) => state.orderCart.userOrder);
  const users = useSelector((state) => state.user);
  const totalAmount = useSelector((state) => state.orderCart.totalAmount);
  let totalPrice = 0;
  for (let order of orders) {
    totalPrice = totalPrice + order.price * order.amount;
  }
  const closeCartHandler = () => {
    dispatch(cartToggleAction.toggleCart());
  };
  const triggerOrderHandler = () => {
    if (!users.isUserLogin) {
      setTriggerOrder(true);
      return;
    } else {
      const sentOrder = {
        userOrder: orders,
        totalAmount: totalAmount,
        totalPrice: totalPrice,
        userInfo: {
          name: users.name,
          tel: users.tel,
          address: users.address,
        },
      };
      sentOrderData(sentOrder, dispatch, closeCartHandler, history);
    }
  };
  const sentNoUserDataOrder = (userInfo) => {
    const sentOrder = {
      userOrder: orders,
      totalAmount: totalAmount,
      totalPrice: totalPrice,
      userInfo,
    };
    sentOrderData(sentOrder, dispatch, closeCartHandler, history);
  };
  return (
    <div className={classes.cartContainer}>
      {orders.map((order) => (
        <Order
          key={order.id}
          name={order.name}
          price={order.price}
          amount={order.amount}
          id={order.id}
        />
      ))}
      <div className={classes.totalPriceContainer}>
        <div className={classes.textTotalPriceContainer}>
          <h3>Total Price</h3>
        </div>
        <div className={classes.numberTotalPriceContainer}>
          <h4>{totalPrice} B</h4>
        </div>
      </div>
      {triggerOrder && (
        <ClientForm
          closeCart={closeCartHandler}
          sentUserData={sentNoUserDataOrder}
        />
      )}
      {!triggerOrder && (
        <div className={classes.buttonContainer}>
          {totalPrice > 0 && (
            <button onClick={triggerOrderHandler}>Order</button>
          )}
          <button onClick={closeCartHandler}>Close</button>
        </div>
      )}
    </div>
  );
};

export default Cart;
