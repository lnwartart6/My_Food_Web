import { useRef } from "react";
import classes from "./ClientForm.module.css";

const ClientForm = (props) => {
  const nameRef = useRef();
  const telRef = useRef();
  const addressRef = useRef();
  const submitFormHandler = (event) => {
    event.preventDefault();
    const userInfo = {
      name: nameRef.current.value,
      tel: telRef.current.value,
      address: addressRef.current.value,
    };
    props.sentUserData(userInfo);
  };
  const closeCartHandler = () => {
    props.closeCart();
  };
  return (
    <form onSubmit={submitFormHandler} className={classes.formContainer}>
      <div className={classes.detailContainer}>
        <label>Name</label>
        <input type="text" ref={nameRef} required />
      </div>
      <div className={classes.detailContainer}>
        <label>Tel.</label>
        <input type="text" ref={telRef} required />
      </div>
      <div className={classes.detailContainer}>
        <label>Address</label>
        <input type="text" ref={addressRef} required />
      </div>
      <div className={classes.buttonContainer}>
        <button>Order</button>
        <button onClick={closeCartHandler}>Close</button>
      </div>
    </form>
  );
};

export default ClientForm;
