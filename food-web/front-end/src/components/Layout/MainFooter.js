import classes from "./MainFooter.module.css";

const MainFooter = () => {
  return (
    <footer className={classes.mainFooter}>
      <h5>Created By Tada Nopparattayaporn</h5>
    </footer>
  );
};

export default MainFooter;
