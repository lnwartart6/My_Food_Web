import { Fragment, useState } from "react";
import ReactDom from "react-dom";
import { useDispatch, useSelector } from "react-redux";

import { cartToggleAction } from "../store/cartToggleSlice";

import MainNavbar from "./MainNavbar";
import MainFooter from "./MainFooter";
import Cart from "../Cart/Cart";
import Overlay from "../Overlay/Overlay";
import classes from "./Layout.module.css";

const Layout = (props) => {
  const dispatch = useDispatch();
  const isCartOpen = useSelector((state) => state.toggleCart.isCartOpen);

  const switchStateCartHandler = () => {
    dispatch(cartToggleAction.toggleCart());
  };

  return (
    <Fragment>
      {isCartOpen &&
        ReactDom.createPortal(
          <Overlay onClickOverlay={switchStateCartHandler} />,
          document.querySelector("#overlay")
        )}
      {isCartOpen &&
        ReactDom.createPortal(<Cart />, document.querySelector("#cartPopup"))}

      <MainNavbar onClickOverlay={switchStateCartHandler} />
      <main className={classes.main}>{props.children}</main>
      <MainFooter />
    </Fragment>
  );
};

export default Layout;
