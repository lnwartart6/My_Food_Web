import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import { userActions } from "../store/userSlice";

import classes from "./MainNavbar.module.css";
import { Fragment } from "react";

const MainNavbar = (props) => {
  const totalAmount = useSelector((state) => state.orderCart.totalAmount);
  const isUserLogin = useSelector((state) => state.user.isUserLogin);
  const dispatch = useDispatch();
  const history = useHistory();
  const triggerCart = () => {
    props.onClickOverlay();
  };
  const logoutHandler = () => {
    dispatch(userActions.logoutUser());
    history.push("/home");
  };
  const profileHandler = () => {
    history.push("/profile");
  };

  return (
    <nav className={classes.controlSizeNavbar}>
      <Link to="/home">Home</Link>
      <Link to="/menu?q=Burger">Menu</Link>
      {!isUserLogin && <Link to="/login">Login</Link>}
      {isUserLogin && (
        <Fragment>
          <button onClick={logoutHandler} className={classes.userLoginButton}>
            Logout
          </button>
          <button onClick={profileHandler} className={classes.userLoginButton}>
            Profile
          </button>
        </Fragment>
      )}
      <button onClick={triggerCart} className={classes.cartButton}>
        Cart <span>{totalAmount}</span>
      </button>
    </nav>
  );
};

export default MainNavbar;
