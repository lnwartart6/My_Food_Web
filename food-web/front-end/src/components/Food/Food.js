import { useRef } from "react";
import { useDispatch } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { cartOrderActions } from "../store/cartOrderSlice";

import classes from "./Food.module.css";

const Food = (props) => {
  const dispatch = useDispatch();
  const { typeFood } = useParams();
  const history = useHistory();
  const { id, name, price, url } = props.dataFood;
  const numberOfFoodRef = useRef();
  const formSubmitHandler = (event) => {
    event.preventDefault();
    const newOrder = {
      id,
      name,
      price,
      amount: parseInt(numberOfFoodRef.current.value),
    };
    dispatch(cartOrderActions.addOrder(newOrder));
    history.push(`/menu?q=${typeFood}`);
  };
  return (
    <div className={classes.foodContainer}>
      <div className={classes.bgFoodPage}></div>
      <div className={classes.foodContent}>
        <div className={classes.pictureContainer}>
          <img src={url} className={classes.sizingImg}></img>
        </div>
        <div className={classes.foodDetailContainer}>
          <h1>{name}</h1>
          <h4>{price} B</h4>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </p>
          <form onSubmit={formSubmitHandler}>
            <input
              type="number"
              defaultValue={1}
              ref={numberOfFoodRef}
              min={1}
              step={1}
            />
            <button>Add to Cart</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Food;
