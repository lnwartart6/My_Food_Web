import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import classes from "./MiniMenu.module.css";
import ListOfMenu from "./ListOfMenu";
import ButtonCategory from "./ButtonCategory";

const listOfCategory = ["Burger", "Chicken", "Fries", "Milk Shakes", "Drinks"];

const MiniMenu = () => {
  const allMenu = useSelector((state) => state.menu);
  const menuLengthSix = {};

  try {
    for (let keyOfAllMenu in allMenu) {
      menuLengthSix[keyOfAllMenu] = allMenu[keyOfAllMenu].slice(0, 6);
    }
  } catch (e) {
    console.log(e);
  }
  const [showMenu, setShowMenu] = useState("Burger");
  const [listOfMenu, setListOfMenu] = useState(menuLengthSix[showMenu]);
  const history = useHistory();
  const onSelectTypeOfMenuHandler = (buttonTarget) => {
    setShowMenu(buttonTarget);
  };
  let showViewMoreButton = false;
  if (listOfMenu.length === 6) {
    showViewMoreButton = true;
  }
  const viewMoreHandler = () => {
    history.push(`/menu?q=${showMenu}`);
  };
  useEffect(() => {
    if (showMenu !== "Milk Shakes") {
      setListOfMenu(menuLengthSix[showMenu]);
    } else {
      setListOfMenu(menuLengthSix["Milk_Shakes"]);
    }
  }, [showMenu]);

  return (
    <div className={classes.miniMenuContainer}>
      <div className={classes.contentContainer}>
        <h1>OUR MENU</h1>
        <div className={classes.miniMenu}>
          <div className={classes.categoryContainer}>
            <div className={classes.buttonContainer}>
              {listOfCategory.map((cate, index) => (
                <ButtonCategory
                  showMenu={showMenu}
                  name={cate}
                  onButtonClick={onSelectTypeOfMenuHandler}
                  key={index}
                />
              ))}
            </div>
          </div>
          <div className={classes.menuContainer}>
            {listOfMenu.map((list) => (
              <ListOfMenu
                name={list.name}
                key={list.id}
                id={list.id}
                price={list.price}
                url={list.url}
                typeFood={showMenu}
              />
            ))}
            {showViewMoreButton && (
              <button
                className={classes.viewMoreButton}
                onClick={viewMoreHandler}
              >
                View more!
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
export default MiniMenu;
