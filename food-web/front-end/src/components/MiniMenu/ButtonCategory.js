const activeButtonStyle = { color: "black", backgroundColor: "white" };

const ButtonCategory = (props) => {
  const onButtonCategoryClick = (event) => {
    props.onButtonClick(event.target.innerText);
  };

  if (props.showMenu === props.name) {
    return (
      <button onClick={onButtonCategoryClick} style={activeButtonStyle}>
        {props.name}
      </button>
    );
  } else {
    return <button onClick={onButtonCategoryClick}>{props.name}</button>;
  }
};

export default ButtonCategory;
