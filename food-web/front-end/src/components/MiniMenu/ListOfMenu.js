import { Link } from "react-router-dom";
import classes from "./ListOfMenu.module.css";

const ListOfMenu = (props) => {
  return (
    <div className={classes.menuList}>
      <div className={classes.menuPictureContainer}>
        <Link to={"/menu/" + props.typeFood + "/" + props.id}>
          <img src={props.url} className={classes.menuPicture}></img>
        </Link>
      </div>
      <div className={classes.textContainer}>
        <span>{props.name}</span>
        <span>{props.price} B</span>
      </div>
    </div>
  );
};

export default ListOfMenu;
