import { useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import classes from "./RecommendMenu.module.css";
import ButtonReccomendMenu from "./ButtonRecommendMenu";
const RecommendMenu = () => {
  const [selectedMenu, setSelectedMenu] = useState(0);
  const showRecMenu = useSelector((state) => state.menu.RecMenu[selectedMenu]);
  const history = useHistory();
  const buttonClickHandler = (selectedIndex) => {
    setSelectedMenu(selectedIndex);
  };
  const userOrderRecMenuHandler = () => {
    history.push("/menu?q=Burger");
  };
  return (
    <div className={classes.recommendMenuContainer}>
      <h1>OUR BESTSELLERS</h1>
      <div className={classes.contentContainer}>
        <div className={classes.leftContainer}>
          <h3>{showRecMenu.name}</h3>
          <h4>{showRecMenu.detail}</h4>
          <h5>{showRecMenu.price} B</h5>
          <button onClick={userOrderRecMenuHandler}>Order Now</button>
        </div>
        <div className={classes.rightContainer}>
          <img src={showRecMenu.url} className={classes.recImg} />
          <div>
            <ButtonReccomendMenu
              buttonClick={buttonClickHandler}
              value={0}
              selectMenu={selectedMenu}
            />
            <ButtonReccomendMenu
              buttonClick={buttonClickHandler}
              value={1}
              selectMenu={selectedMenu}
            />
            <ButtonReccomendMenu
              buttonClick={buttonClickHandler}
              value={2}
              selectMenu={selectedMenu}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default RecommendMenu;
