const ButtonReccomendMenu = (props) => {
  const buttonClickHandler = (event) => {
    props.buttonClick(event.target.value);
  };
  if (parseInt(props.selectMenu) === props.value) {
    return (
      <button
        onClick={buttonClickHandler}
        value={props.value}
        style={{ backgroundColor: "black" }}
      ></button>
    );
  } else {
    return <button onClick={buttonClickHandler} value={props.value}></button>;
  }
};

export default ButtonReccomendMenu;
