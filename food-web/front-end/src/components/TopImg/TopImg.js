import classes from "./TopImg.module.css";

const TopImg = (props) => {
  return (
    <div className={classes.topImgContainer}>
      <img src={props.picture} className={classes.imgBg}></img>
    </div>
  );
};

export default TopImg;
