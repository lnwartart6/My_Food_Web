import { Fragment } from "react";
import classes from "./ImgBg.module.css";
import infoBg from "../../resources/pictures/introBg-blur.jpg";

const ImgBg = () => {
  return (
    <Fragment>
      <div className={classes.bgContainer}>
        <img src={infoBg}></img>
      </div>
      <div className={classes.makeBgDarker}></div>
    </Fragment>
  );
};

export default ImgBg;
