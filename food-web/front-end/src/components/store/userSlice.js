import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isUserLogin: false,
  name: "",
  tel: "",
  address: "",
  username: "",
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    loginUser(state, actions) {
      state.isUserLogin = true;
      //state.idToken = actions.payload.idToken;
      state.name = actions.payload.name;
      state.tel = actions.payload.tel;
      state.address = actions.payload.address;
      state.username = actions.payload.username;
      localStorage.setItem(
        "userToken",
        JSON.stringify(actions.payload.idToken)
      );
    },
    logoutUser(state) {
      state.isUserLogin = false;
      //state.idToken = initialState.idToken;
      state.name = initialState.name;
      state.tel = initialState.tel;
      state.address = initialState.address;
      state.username = initialState.username;
      localStorage.removeItem("userToken");
      clearTimeout();
    },
  },
});

export const userActions = userSlice.actions;

export const userReducer = userSlice.reducer;
