import { configureStore } from "@reduxjs/toolkit";
import { cartToggleReducer } from "./cartToggleSlice";
import { cartOrderReducer } from "./cartOrderSlice";
import { menuReducers } from "./menuSlice";
import { userReducer } from "./userSlice";
import { errorReducer } from "./errorMessageSlice";

const store = configureStore({
  reducer: {
    toggleCart: cartToggleReducer,
    orderCart: cartOrderReducer,
    menu: menuReducers,
    user: userReducer,
    error: errorReducer,
  },
});

export default store;
