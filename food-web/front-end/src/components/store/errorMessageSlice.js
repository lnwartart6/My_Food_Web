import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isError: false,
  message: "",
};

const errorSlice = createSlice({
  name: "error",
  initialState,
  reducers: {
    addError(state, actions) {
      state.message = actions.payload;
      state.isError = true;
    },
  },
});

export const errorActions = errorSlice.actions;
export const errorReducer = errorSlice.reducer;
