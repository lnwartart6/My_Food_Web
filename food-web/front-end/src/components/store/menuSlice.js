import { createSlice } from "@reduxjs/toolkit";
import CustomError from "../../functions/CustomError";

const initialState = {
  Burger: [],
  Chicken: [],
  Fries: [],
  Milk_Shakes: [],
  Drinks: [],
  RecMenu: [],
};

const menuSlice = createSlice({
  name: "allMenu",
  initialState,
  reducers: {
    addData(state, actions) {
      Object.assign(state, actions.payload);
    },
  },
});

export const getMenuData = () => {
  return async (dispatch) => {
    //สมมติส่ง req ได้ res กลับมาเป็น obj
    try {
      const res = await fetch("http://localhost:4000/allMenu");
      const data = await res.json();
      if (!data.message) {
        const menuData = {
          Burger: data[0].Burger,
          Chicken: data[0].Chicken,
          Fries: data[0].Drinks,
          Milk_Shakes: data[0].Milk_Shakes,
          Drinks: data[0].Drinks,
          RecMenu: data[0].RecMenu,
        };
        dispatch(menuSlice.actions.addData(menuData));
        return;
      } else {
        throw new CustomError(400, data.message);
      }
    } catch (e) {
      let errorMessage = e.message;
      if (errorMessage === "Failed to fetch") {
        errorMessage = "Cannot connect to server. Please try again later.";
      }
      throw new CustomError(400, errorMessage);
    }

    //throw new CustomError(400, data.message);
  };
};
export const menuReducers = menuSlice.reducer;
