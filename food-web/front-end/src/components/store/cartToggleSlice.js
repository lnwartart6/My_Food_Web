import { createSlice } from "@reduxjs/toolkit";

const initialValue = {
  isCartOpen: false,
};

const cartToggleSlice = createSlice({
  name: "toggleCart",
  initialState: initialValue,
  reducers: {
    toggleCart(state) {
      state.isCartOpen = !state.isCartOpen;
    },
  },
});

export const cartToggleAction = cartToggleSlice.actions;
export const cartToggleReducer = cartToggleSlice.reducer;
