import { createSlice } from "@reduxjs/toolkit";
import CustomError from "../../functions/CustomError";

const initialState = {
  userOrder: [],
  totalAmount: 0,
};

const cartOrderSlice = createSlice({
  name: "userOrder",
  initialState,
  reducers: {
    addOrder(state, actions) {
      const { id } = actions.payload;
      const findExistingOrder = state.userOrder.find(
        (order) => order.id === id
      );
      if (!findExistingOrder) {
        state.userOrder.push(actions.payload);
        state.totalAmount = state.totalAmount + actions.payload.amount;
      } else {
        findExistingOrder.amount =
          findExistingOrder.amount + actions.payload.amount;
        state.totalAmount = state.totalAmount + actions.payload.amount;
      }
      localStorage.setItem("orderInCart", JSON.stringify(state));
    },
    decreaseFood(state, actions) {
      const id = actions.payload;
      const findExistingOrderIndex = state.userOrder.findIndex(
        (order) => order.id === id
      );
      if (state.userOrder[findExistingOrderIndex].amount > 1) {
        state.userOrder[findExistingOrderIndex].amount--;
      } else {
        state.userOrder.splice(findExistingOrderIndex, 1);
      }
      state.totalAmount--;
      localStorage.setItem("orderInCart", JSON.stringify(state));
    },
    increaseFood(state, actions) {
      const id = actions.payload;
      const findExistingOrder = state.userOrder.find(
        (order) => order.id === id
      );
      findExistingOrder.amount++;
      state.totalAmount++;
      localStorage.setItem("orderInCart", JSON.stringify(state));
    },
    clearFood(state, actions) {
      state.totalAmount = 0;
      state.userOrder = [];
      localStorage.removeItem("orderInCart");
    },
    setInitailFoodInCart(state, actions) {
      state.userOrder = actions.payload.userOrder;
      state.totalAmount = actions.payload.totalAmount;
    },
  },
});

export const sentUserOrder = (dataOrder) => {
  return async (dispatch) => {
    console.log(dataOrder);
    const res = await fetch("http://localhost:4000/order", {
      method: "POST",
      body: JSON.stringify(dataOrder),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!res.ok) {
      const { message } = await res.json();
      const statusCode = res.status;
      throw new CustomError(statusCode, message);
    }
  };
};

export const getOrderInCart = () => {
  return async (dispatch) => {
    const orderJson = localStorage.getItem("orderInCart");
    const orderInCart = JSON.parse(orderJson);
    if (orderJson) {
      dispatch(cartOrderSlice.actions.setInitailFoodInCart(orderInCart));
    } else {
      return;
    }
  };
};

export const cartOrderActions = cartOrderSlice.actions;
export const cartOrderReducer = cartOrderSlice.reducer;
