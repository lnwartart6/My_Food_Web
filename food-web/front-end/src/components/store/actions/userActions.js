import { userActions } from "../userSlice";

export const userLoggingIn = (userData, history) => {
  return async (dispatch) => {
    dispatch(userActions.loginUser(userData));
    setTimeout(() => {
      dispatch(userActions.logoutUser());
      history.push("/home");
    }, 1000 * 60 * 60);
  };
};

export const getUserLogIn = (history) => {
  return async (dispatch) => {
    clearTimeout();
    const userToken = localStorage.getItem("userToken");
    if (userToken) {
      try {
        const userDataToken = {
          idToken: JSON.parse(userToken),
        };
        await fetch("http://localhost:4000/autoLogin", {
          method: "POST",
          body: JSON.stringify(userDataToken),
          headers: {
            "Content-type": "application/json",
          },
        })
          .then((res) => {
            res
              .json()
              .then((data) => {
                if (data.message) {
                  localStorage.removeItem("userToken");
                  alert(data.message);
                  return;
                }
                dispatch(userActions.loginUser(data));
              })
              .catch((e) => {
                console.log(e);
              });
          })
          .catch((e) => {
            console.log(e);
          });
      } catch (e) {
        console.log(e);
        localStorage.removeItem("userToken");
      }
      //sent req to backend to bring user info
      setTimeout(() => {
        dispatch(userActions.logoutUser());
        history.push("/home");
      }, 1000 * 60 * 60);
    } else {
      return;
    }
  };
};
