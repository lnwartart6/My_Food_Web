import classes from "./CategoryButton.module.css";

const buttonLists = ["Burger", "Chicken", "Fries", "Drinks", "Milk Shakes"];
const CategoryButton = (props) => {
  const newLists = buttonLists.filter(
    (buttonList) => buttonList !== props.selectCategory
  );
  const onClickChangeHandler = (event) => {
    props.onChangeCategory(event.target.innerText);
  };

  return (
    <div className={classes.categoryContainer}>
      <div className="btn-group w-50">
        <button
          className="btn btn-secondary dropdown-toggle 
          bg-white text-dark d-flex justify-content-between 
          align-items-center fs-5"
          type="button"
          id="defaultDropdown"
          data-bs-toggle="dropdown"
          data-bs-auto-close="true"
          aria-expanded="false"
        >
          {props.selectCategory}
        </button>
        <ul className="dropdown-menu w-100" aria-labelledby="defaultDropdown">
          <li>
            <button
              className="dropdown-item fs-5"
              href="#"
              onClick={onClickChangeHandler}
            >
              {newLists[0]}
            </button>
          </li>
          <li>
            <button
              className="dropdown-item fs-5"
              href="#"
              onClick={onClickChangeHandler}
            >
              {newLists[1]}
            </button>
          </li>
          <li>
            <button
              className="dropdown-item fs-5"
              href="#"
              onClick={onClickChangeHandler}
            >
              {newLists[2]}
            </button>
          </li>
          <li>
            <button
              className="dropdown-item fs-5"
              href="#"
              onClick={onClickChangeHandler}
            >
              {newLists[3]}
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default CategoryButton;
