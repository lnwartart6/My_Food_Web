import { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import classes from "./Menu.module.css";
import CategoryButton from "./CategoryButton";
import ListOfMenu from "../MiniMenu/ListOfMenu";

const Menu = () => {
  const history = useHistory();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const category = queryParams.get("q") || "Burger";
  const allMenu = useSelector((state) => state.menu);
  const [listOfMenu, setListOfMenu] = useState(allMenu[category]);
  const onClickChangeCategory = (targetText) => {
    history.push(`/menu?q=` + targetText);
  };

  useEffect(() => {
    if (category === "Burger") {
      setListOfMenu(allMenu.Burger);
    } else if (category === "Chicken") {
      setListOfMenu(allMenu.Chicken);
    } else if (category === "Fries") {
      setListOfMenu(allMenu.Fries);
    } else if (category === "Milk Shakes") {
      setListOfMenu(allMenu.Milk_Shakes);
    } else if (category === "Drinks") {
      setListOfMenu(allMenu.Drinks);
    }
  }, [category]);

  return (
    <div className={classes.menuContainer}>
      <h1>Home &#62; Our Menu</h1>
      <div className={classes.bgMenu}></div>
      <div className={classes.contentContainer}>
        <CategoryButton
          onChangeCategory={onClickChangeCategory}
          selectCategory={category}
        />
        <div className={classes.showMenuContainer}>
          {listOfMenu.map((list) => (
            <ListOfMenu
              name={list.name}
              key={list.id}
              id={list.id}
              price={list.price}
              url={list.url}
              typeFood={category}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Menu;
