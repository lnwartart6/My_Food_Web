import { useHistory } from "react-router-dom";

import classes from "./Intro.module.css";
import infoBg from "../../resources/pictures/introBg-blur.jpg";
import ImgBg from "../TopImg/ImgBg";

const Intro = () => {
  const history = useHistory();
  const viewMenuHandler = () => {
    history.push("/menu");
  };

  return (
    <div className={classes.introContainer}>
      <ImgBg />
      <div className={classes.contentContainer}>
        <h1>HUNGRY?</h1>
        <div className={classes.textContainer}>
          <h2>IT'S</h2>
          <h2>DELICIOUS</h2>
        </div>
        <h4>Hurry Order Food</h4>
        <div className={classes.buttonContainer}>
          <button onClick={viewMenuHandler}>View Menu</button>
        </div>
      </div>
    </div>
  );
};

export default Intro;
