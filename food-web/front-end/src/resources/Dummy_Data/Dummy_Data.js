// ข้อมูล Menu
export const Dummy_menu_burger = [
  {
    id: "b1",
    name: "Burger1",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b2",
    name: "Burger2",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b3",
    name: "Burger3",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b4",
    name: "Burger4",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b5",
    name: "Burger5",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b6",
    name: "Burger6",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b7",
    name: "Burger4",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b8",
    name: "Burger5",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b9",
    name: "Burger6",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];
export const Dummy_menu_chicken = [
  {
    id: "c1",
    name: "Chick1",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "c2",
    name: "Chick2",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "c3",
    name: "Chick3",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];

//ข้อมูล RecommendMenu
export const Dummy_ReccomendMenu = [
  {
    id: 1,
    name: "Burger",
    detail:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    price: "10B",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: 2,
    name: "Chick",
    detail:
      "typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    price: "11B",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: 3,
    name: "Drink",
    detail: "Ipsum.",
    price: "12B",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];

//สมมติข้อมูลใน DB

const dummy_db = {
  Burger: [],
  Chicken: [],
  Fries: [],
  Milk_Shakes: [],
  Drinks: [],
  RecMenu: [],
};
