const jwt = require("jsonwebtoken");
const User = require("../models/userSchema/userSchema");
const ExpressError = require("../utils/ExpressError");

module.exports = async (req, res, next) => {
  const { idToken } = req.body;
  console.log(idToken);
  try {
    const decoded = jwt.verify(idToken, "userSecret");
    console.log(decoded);
    try {
      const userInDb = await User.findById({ _id: decoded.user_id });
      const responseBack = {
        username: userInDb.username,
        name: userInDb.name,
        tel: userInDb.tel,
        address: userInDb.address,
        idToken,
      };
      return res.send(responseBack);
    } catch (e) {
      next(new ExpressError("You are not authorized", 400));
    }
  } catch (e) {
    next(new ExpressError(e.message, 400));
  }
};
