const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require("../models/userSchema/userSchema");
const ExpressError = require("../utils/ExpressError");

module.exports = async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const userInDb = await User.findOne({ username });
    if (userInDb) {
      const result = await bcrypt.compare(password, userInDb.password);
      if (result) {
        const token = jwt.sign(
          { user_id: userInDb._id, username },
          "userSecret",
          {
            expiresIn: "6h",
          }
        );
        userInDb.token = token;
        userInDb.save();
        const responseBack = {
          idToken: token,
          name: userInDb.name,
          tel: userInDb.tel,
          address: userInDb.address,
          username: userInDb.username,
        };
        res.send(responseBack);
      } else {
        res.send({ message: "Username or Password is not correct" });
      }
    } else {
      res.send({ message: "Username or Password is not correct" });
    }
  } catch (e) {
    console.log("throw error");
    next(new ExpressError("Cannot connect to server pls try again", 400));
  }
};

// name: userInDb.name,
// address: userInDb.address,
// tel: userInDb.tel,
// username: userInDb.username,
