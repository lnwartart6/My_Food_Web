const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/userSchema/userSchema");

module.exports = async (req, res) => {
  const { username, password, name, tel, address } = req.body;
  console.log(req.body);
  const newUserData = {
    username,
    password,
    name,
    tel,
    address,
  };

  try {
    const existingUsername = await User.findOne({ username });
    if (existingUsername) {
      res.send({ message: "Username already exist!" });
      return;
    }

    const hashPassword = async (pw) => {
      const salt = await bcrypt.genSalt(12);
      const hash = await bcrypt.hash(pw, salt);
      newUserData.password = hash;
    };
    hashPassword(newUserData.password).then(() => {
      const newUser = new User(newUserData);
      const token = jwt.sign({ user_id: newUser._id, username }, "userSecret", {
        expiresIn: "6h",
      });
      newUser.token = token;
      newUser.save();
      const responseBack = {
        isUserLogin: true,
        idToken: token,
      };
      res.send(responseBack);
    });
  } catch (e) {
    console.log("throw error");
    next(new ExpressError("Cannot connect to server pls try again", 400));
  }
};
