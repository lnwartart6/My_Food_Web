const Menu = require("../models/menuSchema/menuSchema");
const ExpressError = require("../utils/ExpressError");

module.exports = async (req, res, next) => {
  try {
    const allMenu = await Menu.find({});
    res.send(allMenu);
  } catch (e) {
    next(new ExpressError(e.message, 400));
  }
};
