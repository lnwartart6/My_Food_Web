const User = require("../models/userSchema/userSchema");
const bcrypt = require("bcrypt");
const ExpressError = require("../utils/ExpressError");

module.exports = async (req, res, next) => {
  const { address, name, password, tel } = req.body;
  const { user_id, username } = req.user;
  try {
    const user = await User.findById({ _id: user_id });
    const result = await bcrypt.compare(password, user.password);
    if (result) {
      //pw ตรงให้อัพเดตได้
      const temp = await User.findOneAndUpdate(
        { username },
        { name, address, tel },
        { new: true }
      );
      res.json({ sucess: "ok" });
    } else {
      //pw ไม่ตรง
      next(new ExpressError("Password Incorrect", 403));
    }
  } catch (e) {
    next(new ExpressError("Token Invalid", 403));
  }
};
