module.exports.Burger = [
  {
    id: "b1",
    name: "Burger1",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b2",
    name: "Burger2",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b3",
    name: "Burger3",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b4",
    name: "Burger4",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b5",
    name: "Burger5",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b6",
    name: "Burger6",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b7",
    name: "Burger4",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b8",
    name: "Burger5",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "b9",
    name: "Burger6",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];
module.exports.Chicken = [
  {
    id: "c1",
    name: "Chick1",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "c2",
    name: "Chick2",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "c3",
    name: "Chick3",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];
module.exports.Fries = [
  {
    id: "f1",
    name: "fries1",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "f2",
    name: "fries2",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "f3",
    name: "fries3",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];
module.exports.Drinks = [
  {
    id: "d1",
    name: "drink1",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "d2",
    name: "drink2",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "d3",
    name: "drink3",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];
module.exports.Milk_Shakes = [
  {
    id: "m1",
    name: "milkS1",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "m2",
    name: "milkS2",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "m3",
    name: "milkS3",
    price: 10,
    detail: "detail",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];

module.exports.ReccomendMenu = [
  {
    id: "b1",
    name: "Burger1",
    price: 10,
    detail:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "c1",
    name: "Chick1",
    price: 10,
    detail:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
  {
    id: "d1",
    name: "drink1",
    price: 10,
    detail:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    url: "https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg",
  },
];
