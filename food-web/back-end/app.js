const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const orderRouters = require("./routes/orders");
const allMenuRouter = require("./routes/allMenu");
const loginRouter = require("./routes/login");
const autoLoginRouter = require("./routes/autoLogin");
const registerRouter = require("./routes/register");
const profileRouter = require("./routes/profile");

mongoose
  .connect("mongodb://localhost:27017/food-web", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("success");
  })
  .catch((e) => {
    console.log(e);
  });
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/allMenu", allMenuRouter);
app.use("/order", orderRouters);
app.use("/login", loginRouter);
app.use("/autoLogin", autoLoginRouter);
app.use("/register", registerRouter);
app.use("/profile", profileRouter);

app.use((err, req, res, next) => {
  console.log("In error middleware");
  console.log(err.statusCode);
  console.log(err.message);
  const errorResponse = {
    message: err.message,
  };
  res.status(err.statusCode).send(errorResponse);
});

app.listen(4000, () => {
  console.log("hello in port 4000");
});
