const express = require("express");
const router = express.Router();
const userLogin = require("../controllers/loginLogic");

router.post("/", userLogin);

module.exports = router;
