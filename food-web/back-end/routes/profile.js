const express = require("express");
const router = express.Router();
const verifyToken = require("../middlewares/verifyToken");
const userUpdateProfile = require("../controllers/updateProfileLogic");

router.patch("/", verifyToken, userUpdateProfile);

module.exports = router;
