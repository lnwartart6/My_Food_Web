const express = require("express");
const router = express.Router();
const { validateUser } = require("../joiSchema");
const userRegister = require("../controllers/registerLogic");

router.post("/", validateUser, userRegister);

module.exports = router;
