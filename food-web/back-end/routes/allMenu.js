const express = require("express");
const router = express.Router();
const getAllMenuLogic = require("../controllers/getAllMenuLogic");

router.get("/", getAllMenuLogic);

module.exports = router;
