const express = require("express");
const router = express.Router();
const ExpressError = require("../utils/ExpressError");
const Order = require("../models/orderSchema/orderSchema");
const { validateOrder } = require("../joiSchema");

router.get("/", async (req, res) => {
  //เผื่อไว้ใช้ดู order ของลูกค้า(ยังไม่ได้นำไปใช้ในเว็บ)
  const allOrder = await Order.find({});
  res.send(allOrder);
});

router.post("/", validateOrder, async (req, res, next) => {
  try {
    const userInputOrder = req.body;
    const newUserOrder = new Order(userInputOrder);
    newUserOrder.save();
    res.send("Ok");
  } catch (e) {
    next(new ExpressError("Can not sent Order", 400));
  }
});

module.exports = router;
