const express = require("express");
const router = express.Router();
const autoLogin = require("../controllers/autoLoginLogic");

router.post("/", autoLogin);

module.exports = router;
