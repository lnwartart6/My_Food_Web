const jwt = require("jsonwebtoken");
const ExpressError = require("../utils/ExpressError");
module.exports = async (req, res, next) => {
  console.log("in verifyToken middleware");
  const token =
    req.body.idToken ||
    req.query.idToken ||
    req.headers["x-access-token"] ||
    "sadasdasdcmvlxcmvsefdsdf";
  try {
    const decoded = jwt.verify(token, "userSecret");
    console.log(decoded);
    req.user = decoded;
  } catch (err) {
    console.log("error");
    next(new ExpressError("Invalid Token", 403));
    return;
  }
  return next();
};
