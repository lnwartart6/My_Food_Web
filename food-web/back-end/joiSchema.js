const Joi = require("joi");
const ExpressError = require("./utils/ExpressError");

const userSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
  name: Joi.string().required(),
  tel: Joi.string().required(),
  address: Joi.string().required(),
  token: Joi.string(),
}).required();

const orderSchema = Joi.object({
  userOrder: Joi.array()
    .items({
      id: Joi.string().required(),
      name: Joi.string().required(),
      amount: Joi.number().required(),
      price: Joi.number().required(),
    })
    .required(),
  userInfo: Joi.object({
    name: Joi.string().required(),
    tel: Joi.string().required(),
    address: Joi.string().required(),
  }).required(),
  totalPrice: Joi.number().required(),
  totalAmount: Joi.number().required(),
}).required();

module.exports.validateUser = (req, res, next) => {
  const result = userSchema.validate(req.body);
  if (result.error) {
    const msg = result.error.details.map((el) => el.message).join(",");
    throw new ExpressError(msg, 400);
  }
  next();
};

module.exports.validateOrder = (req, res, next) => {
  const result = orderSchema.validate(req.body);
  if (result.error) {
    const msg = result.error.details.map((el) => el.message).join(",");
    throw new ExpressError(msg, 400);
  }
  next();
};
