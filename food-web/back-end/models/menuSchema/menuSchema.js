const mongoose = require("mongoose");

const configSchema = {
  id: String,
  name: String,
  detail: String,
  price: Number,
  url: String,
};

const menuSchema = new mongoose.Schema({
  Burger: [configSchema],
  Chicken: [configSchema],
  Fries: [configSchema],
  Milk_Shakes: [configSchema],
  Drinks: [configSchema],
  RecMenu: [configSchema],
});

const Menu = mongoose.model("Menu", menuSchema);

module.exports = Menu;
