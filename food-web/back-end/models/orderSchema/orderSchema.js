const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
  userOrder: [
    {
      id: { type: String, required: true },
      name: { type: String, required: true },
      amount: { type: Number, required: true },
      price: { type: Number, required: true },
    },
  ],
  userInfo: {
    name: { type: String, required: true },
    tel: { type: String, required: true },
    address: { type: String, required: true },
  },
  totalPrice: { type: Number, required: true },
  totalAmount: { type: Number, required: true },
});

const Order = mongoose.model("userOrder", orderSchema);

module.exports = Order;
