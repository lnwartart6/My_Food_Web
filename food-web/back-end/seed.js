const express = require("express");
const app = express();
const mongoose = require("mongoose");
const allMenu = require("./dummy_menu");

const Menu = require("./models/menuSchema/menuSchema");

console.log(allMenu);

mongoose
  .connect("mongodb://localhost:27017/food-web")
  .then(() => {
    console.log("success");
  })
  .catch((e) => {
    console.log(e);
  });

async function seedAllMenu() {
  const menuData = {
    Burger: allMenu.Burger,
    Chicken: allMenu.Chicken,
    Fries: allMenu.Drinks,
    Milk_Shakes: allMenu.Milk_Shakes,
    Drinks: allMenu.Drinks,
    RecMenu: allMenu.ReccomendMenu,
  };
  const newMenu = new Menu(menuData);
  newMenu.save();
}

async function clearAllMenu() {
  await Menu.deleteMany({});
}

app.listen(4000, () => {
  console.log("hello in port 4000");
});
clearAllMenu().then(() => {
  seedAllMenu();
});
